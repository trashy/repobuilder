# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Johan `trashy` Cwiklinski
# This file is distributed under the same license as the RepoBuilder package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: RepoBuilder 0.0.3\n"
"Report-Msgid-Bugs-To: johan@x-tnd.be\n"
"POT-Creation-Date: 2016-08-30 09:57+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/builder/repobuilder.git/repobuilder.py:51
msgid ""
"Special thanks to pingou and number80 on IRC for their precious help and "
"their wonderful knowledge :)\n"
"Many thanks also to my wife and my daughter."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:52
msgid "Project on BitBucket"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:84
msgid "Cannot find repoview templates, aborting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:106
msgid "No module has been selected. Exiting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:109
#, python-format
msgid "Loaded modules: %s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:189
msgid ""
"You have not selected any rpmbuild action. Do you want to run default action "
"(-bb)? [Y/n]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:191
msgid "No rpmbuild action specified, exiting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:204
#, python-format
msgid "All generated RPMs and SRPMs has been cleaned for module %s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:248
msgid ""
"You have not selected any repo action. Do you want to run default action "
"(complete)? [Y/n]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:250
msgid "No repo action specified, exiting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:292
#, python-format
msgid "Valid module: specfile (%s) do exist on the disk."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:294
#, python-format
msgid ""
"Specfile (%s) does not exists. Maybe you do not specify a valid module for "
"repobuilder. Continue anyway? [y/N]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:296
msgid "Not a module. Please insert coin."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:298
msgid "Not found."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:331
#, python-format
msgid "Initialized module: %(name)s - file: %(file)s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:343
msgid "Running mock for current environment only"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:376
#, python-format
msgid "Checking if sources exists for module %s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:378
msgid "Command: "
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:394
msgid "Getting sources"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:400
msgid "An error occurred getting the sources. Do you want to continue? [Y/n]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:402
#, python-format
msgid "Cannot retrieve %s's sources."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:441
msgid ""
"More than one src.rpm has been found... Do you want to remove them all and "
"rebuild srpm? [y/n]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:447
msgid "Multiple SRPMs has been found, I cannot choose the right one, aborting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:459
#, python-format
msgid "Removing SRPM %s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:471
#, python-format
msgid "Cleaning `%s`"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:474
#, python-format
msgid "%s is in ignore-list, ignored."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:479
#, python-format
msgid "Trying to delete `%s`"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:523
#, python-format
msgid "Unable to build %(arch)s packages on a %(current_arch)s machine."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:552
#, python-format
msgid "Mock resultdir `%s` cleaned"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:555
#, python-format
msgid "Mock ccache dir `%s` cleaned"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:609
#, python-format
msgid "Searching for RPMs in %s"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:621
msgid "Entered passphrase is incorrect. Do you have another guy? [Y/n]"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:623
msgid "Bad GPG passphrase, aborting."
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:625
msgid "Unable to sign package!"
msgstr ""

#: /home/builder/repobuilder.git/repobuilder.py:753
#, python-format
msgid "Local repository path does not exists (%s)"
msgstr ""

#: /home/builder/repobuilder.git/grepobuilder.py:79
msgid "Modules"
msgstr ""

#: /home/builder/repobuilder.git/grepobuilder.py:90
#, python-format
msgid "Retrieving informations for module %s"
msgstr ""

#: /home/builder/repobuilder.git/grepobuilder.py:111
#, python-format
msgid "Information retrieved for module %s"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:7
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:104
msgid "Configuration"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:36
msgid "Sources path"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:53
#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:147
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:367
msgid "Path"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:63
msgid "Dist"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:73
msgid "Repoview tpml"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:131
msgid "Host"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:137
msgid "User"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:196
msgid "Remote repository"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:213
msgid "<b>Repository</b>"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:228
msgid "OK"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_config_dialog.glade:241
msgid "Cancel"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:15
msgid "RepoBuilder - GTK"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:29
msgid "_RepoBuilder"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:43
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:103
msgid "RepoBuilder's preferences"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:63
msgid "_Help"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:126
msgid "Quit"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:186
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:202
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:221
#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:240
msgid "label"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:193
msgid "rpmbuild"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:211
msgid "mock"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:230
msgid "sync"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:249
msgid "clean"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:291
msgid "Name"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:301
msgid "Releases"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:315
msgid "Architectures"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:340
msgid "Specfile"
msgstr ""

#: /home/builder/repobuilder.git/glade/grepobuilder_main.glade:421
msgid "<b>Module informations</b>"
msgstr ""
