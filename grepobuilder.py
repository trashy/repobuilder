#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 Johan Cwiklinski <johan AT x-tnd DOT be>
#
# This file is part of repobuilder.
#
# repobuilder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# repobuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with repobuilder.  If not, see <http://www.gnu.org/licenses/>.

"""
Fedora Repository Builder - GTK GUI
"""

import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade
import repobuilder
import os
from repobuilder import Repository
from repobuilder import Module

import gettext

#that way, GUI is not translated (but should be)
#gettext.install(repobuilder.application, repobuilder.locale_dir)

#solution found there: http://www.daa.com.au/pipermail/pygtk/2007-March/013586.html
#locale.setlocale(locale.LC_ALL, '')
# see http://bugzilla.gnome.org/show_bug.cgi?id=344926 for why the
# next two commands look repeated.
gtk.glade.bindtextdomain(repobuilder.application, repobuilder.locale_dir)
gtk.glade.textdomain(repobuilder.application)
gettext.bindtextdomain(repobuilder.application, repobuilder.locale_dir)
gettext.textdomain(repobuilder.application)

class GRepoBuilder(object):
    def __init__(self):
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.repo = Repository()
        infos = self.repo.currentHostInfos()

        self.builder = gtk.Builder()
        self.builder.add_from_file(os.path.join(self.path, 'glade/grepobuilder_main.glade'))
        self.builder.set_translation_domain(repobuilder.application)

        self.window = self.builder.get_object('main_window')
        self.window.set_title('%s - On %s %s/%s' % (self.window.get_title(), infos['fullname'], infos['cleaned_release'], infos['arch']))
        self.builder.connect_signals(self)

        self.statusbar = self.builder.get_object('statusbar')
        self.statusbar.push(0, 'RepoBuilder running on %s %s/%s' % (infos['fullname'], infos['cleaned_release'], infos['arch']))

        self._build_modules_list()

    def _build_modules_list(self):
        modules_view = self.builder.get_object('modules_view')
        modules_store = self.builder.get_object('modules_store')

        selection = modules_view.get_selection()
        selection.connect('changed', self.on_selection_changed)

        sel_column = gtk.TreeViewColumn(None, gtk.CellRendererToggle())
        sel_column.set_resizable(False)
        sel_column.set_sort_column_id(0)
        modules_view.append_column(sel_column)

        column = gtk.TreeViewColumn(_('Modules'), gtk.CellRendererText(), text=1)
        column.set_resizable(True)
        column.set_sort_column_id(1)
        modules_view.append_column(column)

        for mod in self.repo.modulesList():
            modules_store.append([False, mod])

    def on_selection_changed(self, selection):
        model, paths = selection.get_selected_rows()
        if paths:
            self.statusbar.push(0, _('Retrieving informations for module %s') % model[paths[0]][1])
            mod = Module(model[paths[0]][1], None, self.repo)

            module_name = self.builder.get_object('module_name')
            module_name.set_text(mod.name)

            module_path = self.builder.get_object('module_path')
            module_path.set_text(mod.path)

            module_specfile = self.builder.get_object('module_specfile')
            module_specfile.set_text(mod.specfile)

            module_name = self.builder.get_object('module_name')
            module_name.set_text(mod.name)

            module_releases = self.builder.get_object('module_releases')
            module_releases.set_text(', '.join(mod.releases))

            module_arches = self.builder.get_object('module_arches')
            module_arches.set_text(', '.join(mod.arches))

            self.statusbar.push(0, _('Information retrieved for module %s') % mod.name)

    def on_config_btn_clicked(self, widget, data=None):
        """
        To open app configuration window
        """
        config_dialog = configDialog(self.repo)
        result = config_dialog.run()

    def on_about_mnu_activate(self, widget, data=None):
        about = gtk.AboutDialog()
        about.set_name(repobuilder.name)
        about.set_version(repobuilder.version)
        about.set_copyright(repobuilder.copyright)
        about.set_comments(repobuilder.credits)
        about.set_license(open('gpl.txt').read())
        about.set_website(repobuilder.website)
        about.set_website_label(repobuilder.websitelabel)
        about.set_authors(repobuilder.authors)
        about.set_logo(gtk.gdk.pixbuf_new_from_file(os.path.join(self.path, 'images/logo.png')))
        about.run()

    def gtk_widget_destroy(self, widget, data=None):
        pass

    def on_main_window_destroy(self, widget, data=None):
        gtk.main_quit()

class configDialog:
    """
    Configuration window
    """
    def __init__(self, repo):
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.repo = repo
        self.builder = gtk.Builder()
        self.builder.set_translation_domain(repobuilder.application)
        self.builder.add_from_file(os.path.join(self.path, 'glade/grepobuilder_config_dialog.glade'))

        self.window = self.builder.get_object('config_window')
        infos = self.repo.currentHostInfos()
        self.window.set_title('%s - On %s %s/%s' % (self.window.get_title(), infos['fullname'], infos['cleaned_release'], infos['arch']))
        self.builder.connect_signals(self)

    def run(self):
        """
        Populate and show configuration dialog
        """
        repo_src_path = self.builder.get_object('repo_src_path')
        repo_src_path.set_text(self.repo.repo_src_path)

        repo_path = self.builder.get_object('repo_path')
        repo_path.set_text(self.repo.local)

        repo_dist = self.builder.get_object('repo_dist')
        repo_dist.set_text(self.repo.dist)

        repoview_tmpl = self.builder.get_object('repoview_tmpl')
        repoview_tmpl.set_text(self.repo.repoview_tmpl)

        repo_remote_host = self.builder.get_object('repo_remote_host')
        repo_remote_host.set_text(self.repo.remote['host'])
        repo_remote_user = self.builder.get_object('repo_remote_user')
        repo_remote_user.set_text(self.repo.remote['user'])
        repo_remote_path = self.builder.get_object('repo_remote_path')
        repo_remote_path.set_text(self.repo.remote['path'])

        #run the dialog and store the response
        self.result = self.window.show()

        return self.result

    def on_config_window_destroy(self, widget, data=None):
       pass

if __name__ == '__main__':
    app = GRepoBuilder()
    app.window.show()
    gtk.main()