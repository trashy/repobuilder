#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011-2016 Johan Cwiklinski <johan AT x-tnd DOT be>
#
# This file is part of repobuilder.
#
# repobuilder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# repobuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with repobuilder.  If not, see <http://www.gnu.org/licenses/>.

"""
Fedora Repository Builder

A simple script to made maintaining a Fedora/EPEL repository easier
"""

import os
import sys
import re
import argparse
import platform
import glob
import configparser
import subprocess
import copy

if os.name == 'nt':
    print('Really?')
    sys.exit(0)

name = 'RepoBuilder'
version = '0.0.3'
authors = ['Johan `trashy` Cwiklinski']
author_mail = 'johan@x-tnd.be'
locale_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'locale')
copyright = 'Copyright (c) 2011-2016 Johan Cwiklinski'
website = 'https://bitbucket.org/trashy/repobuilder/'

import gettext
application = 'repobuilder'
gettext.install(application, locale_dir)
credits = _('Special thanks to pingou and number80 on IRC for their precious help and their wonderful knowledge :)\nMany thanks also to my wife and my daughter.')
websitelabel = _('Project on BitBucket')

class Repository:

    def __init__(self, modules = None, cmd_line = False):
        global args

        #self.repo_src_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))

        config = configparser.ConfigParser()
        #we read main configuration file only here
        self.config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'repo.cfg')
        config.read(self.config_file)

        #rstrip takes care of ending slash which can cause issues
        self.repo_src_path  = config.get('main configuration', 'modules_src_path').rstrip('/')
        #now we know were the source repository is ; let's look if it ships a repo.cfg in the common repository
        self.repo_config_file = os.path.join(self.repo_src_path, 'common', 'repo.cfg')
        if os.path.exists(self.repo_config_file):
            config.read(self.repo_config_file)
        self.remote = {
                       'host': config.get('remote repository', 'host'),
                       'user': config.get('remote repository', 'user'),
                       'path': config.get('remote repository', 'repo_path')
                       }
        self.local = config.get('local repository', 'repo_path')
        self.dist = config.get('main configuration', 'dist')

        repoview_tmpl = config.get('repoview', 'template_path')
        if not os.path.exists(repoview_tmpl):
            if os.path.exists(os.path.join('..', repoview_tmpl)):
                self.repoview_tmpl = os.path.join('..', repoview_tmpl)
            else:
                sys.exit(_('Cannot find repoview templates, aborting.'))
        else:
            self.repoview_tmpl = repoview_tmpl

        self.repo_config = config
        self.mock_config_name = config.get('mock configuration', 'config_name')
        self.releases = config.get('mock configuration', 'fc')
        self.arches = config.get('mock configuration', 'arch')

        self.current_arch = platform.machine()
        m = re.search('.*fc(\d(2)).*', str(platform.uname()))
        self.current_release = m.group(1);

        #If we're called from command line, let's process
        if cmd_line:
            self.modules = None
            if modules != None:
                self.modules = [Module(mod, os.path.join(os.getcwd(), mod), self) for mod in modules]
            else:
                #if current directory is under the repository, consider we're building the current module
                if os.path.abspath(os.path.join(os.getcwd(), '..')) == self.repo_src_path:
                    self.modules = [Module(os.path.basename(os.getcwd()), os.getcwd(), self)]

            if self.modules == None:
                sys.exit(_('No module has been selected. Exiting.'))
            else:
                if args.verbose:
                    print(_('Loaded modules: %s') % ' '.join(m.name for m in self.modules))

    def modulesList(self):
        """
        List modules contained in the repo
        """
        ignore_list = ['README', '.hg', 'common', '.settings', '.pydevproject', 'mock_config', 'repoview_ulysses_theme', '.project', '.hgignore', '.hgtags']
        modules = []
        dirs = os.listdir(self.repo_src_path)
        dirs.sort()
        for each in dirs:
            if not each in ignore_list:
                modules.append(each)
        return modules

    def sync(self):
        """
        Synchronize local repository with remote one
        """
        global args

        sync_cmd = [
            'rsync',
            '--recursive',
            '--times',
            '--progress',
            '--human-readable',
            '--delay-updates',
            '--no-motd',
            '--delete',
            '--links'
        ]

        if args.bwlimit:
            sync_cmd.append('--bwlimit=%s' % args.bwlimit)

        if args.verbose:
            sync_cmd.append('--verbose')

        sync_cmd.extend([self.local, '%s@%s:%s' % (self.remote['user'], self.remote['host'], self.remote['path'])])

        if args.debug:
            print(sync_cmd)
        subprocess.call(sync_cmd)

    def currentHostInfos(self):
        if self.current_release.startswith('el'):
            osname = 'el'
            osfullname = 'Enterprise Linux'
            osrel = self.current_release[2:]
        else:
            osname = 'fedora'
            osfullname = 'Fedora'
            osrel = self.current_release
        host = {
                'name': osname,
                'fullname': osfullname,
                'release': self.current_release,
                'cleaned_release': osrel,
                'arch': self.current_arch
                }
        return host


    def _doRpmbuildAction(self):
        global args

        if args.rpmbuildaction or args.sources or args.deps:
            for mod in self.modules:
                if args.keepsrpm == False:
                    mod._clean_srpms()

                if args.sources:
                    mod.getSources()
                if args.deps:
                    mod.getDeps()
                if args.rpmbuildaction:
                    mod.rpmbuild(args.rpmbuildaction)
        else:
            r = input(_('You have not selected any rpmbuild action. Do you want to run default action (-bb)? [Y/n]'))
            if r.lower() == 'n':
                sys.exit(_('No rpmbuild action specified, exiting.'))
            elif r.lower() == 'y':
                for mod in self.modules:
                    if args.keepsrpm == False:
                        mod._clean_srpms()

                    mod.rpmbuild('-bb')

    def _doCleanAllAction(self):
        for mod in self.modules:
            #first clean builded RPMs
            mod._recursive_clean(os.path.join(mod.path, 'noarch'))
            mod._recursive_clean(os.path.join(mod.path, 'x86_64'))
            mod._recursive_clean(os.path.join(mod.path, 'i386'))
            #remove SRPMs
            mod._clean_srpms()
            print(_('All generated RPMs and SRPMs has been cleaned for module %s') % mod.name)
            mod.cleanmock()

    def _doCompleteAction(self):
        #cleaning
        self._doCleanAllAction()
        #build SRPM
        args.rpmbuildaction = '-bs'
        args.sources = True
        args.deps = False
        self._doRpmbuildAction()
        #mock
        args.srcrpm = False
        self._doMockAction()
        #build repository
        args.createrepo = False
        args.repoview = False
        args.copy = False
        args.sign = False
        args.sync = False
        args.completerepo = True
        self._doRepoAction()

    def _doRepoAction(self):
        global args

        if args.createrepo:
            for mod in self.modules:
                mod.createRepo()
        elif args.repoview:
            for mod in self.modules:
                mod.repoview()
        elif args.copy:
            for mod in self.modules:
                mod.copyInRepo()
        elif args.sign:
            for mod in self.modules:
                mod.sign()
        elif args.sync:
            for mod in self.modules:
                mod.sync()
        else:
            #complete repo action
            if not args.completerepo:
                r = input(_('You have not selected any repo action. Do you want to run default action (complete)? [Y/n]'))
                if not r.lower() == 'y':
                    sys.exit(_('No repo action specified, exiting.'))

            for mod in self.modules:
                self._repo(mod)

                #ask user confirmation if option has not been specified

    def _repo(self, mod):
        # -s => -c => -cr => -r
        mod.sign()
        mod.copyInRepo()
        mod.createRepo()
        mod.repoview() 

    def _doSyncAction(self):
        self.sync()

    def _doMockAction(self):
        for mod in self.modules:
            if args.srcrpm:
                mod.mock(args.srcrpm)
            else:
                mod.mock()

    def _doLintAction(self):
        for mod in self.modules:
            mod.lint()

class Module:

    def __init__(self, name, path = None, repo = None):
        self.name = name
        self.specfile = '%s.spec' % self.name
        self.repo = repo
        if path != None:
            self.path = path
        else:
            self.path = os.path.join(self.repo.repo_src_path, self.name)

        if os.path.exists(os.path.join(self.path, self.specfile)):
            self.path = os.path.join(self.repo.repo_src_path, self.name)
            if 'args' in globals() and args.verbose:
                print(_('Valid module: specfile (%s) do exist on the disk.') % os.path.join(self.path, self.specfile))
        else:
            r = input(_('Specfile (%s) does not exists. Maybe you do not specify a valid module for repobuilder. Continue anyway? [y/N]') % os.path.join(self.path, self.specfile))
            if r.lower() != 'y':
                sys.exit(_('Not a module. Please insert coin.'))
            else:
                self.specfile = _('Not found.')

        config = repo.repo_config
        local_config = os.path.abspath(os.path.join(self.path, 'repo.cfg'))
        config.read(local_config)

        self.mock_config = config.get('mock configuration', 'config_name')
        self.releases = config.get('mock configuration', 'fc').split(',')
        self.arches = config.get('mock configuration', 'arch').split(',')

        #rpmbuild arguments for RPMs
        self.rpmdefines = [
                '--define', '_sourcedir %s' % self.path,
                '--define', '_specdir %s' % self.path,
                '--define', '_srcrpmdir %s' % self.path,
                '--define', '_rpmdir %s' % self.path
            ]

        #rpmbuild arguments for sources
        self.srcdefines = [
                '--define', 'dist .%s' % (self.repo.dist),
                '--define', '_source_filedigest_algorithm 1',
                '--define', '_binary_filedigest_algorithm 1'
            ]

        #build common rpmbuild command
        self.rpmbuild_cmd = ['rpmbuild']
        self.rpmbuild_cmd.extend(self.rpmdefines)
        self.rpmbuild_cmd.extend(self.srcdefines)

        self._setArchesVersions()

        print(_('Initialized module: %(name)s - file: %(file)s') % {'name': self.name, 'file': self.specfile})

    def _setArchesVersions(self):
        """
        Set selected arches and releases
        """
        global args
        self.selected_releases = None
        self.selected_arches = None

        if 'args' in globals() and hasattr(args, 'local'):
            if args.local:
                print(_('Running mock for current environment only'))
                self.selected_releases = self.repo.current_release.split(',')
                self.selected_arches = self.repo.current_arch.split(',')
            else:
                if hasattr(args, 'releases'):
                    if args.releases:
                        self.selected_releases = args.releases
                if hasattr(args, 'arches'):
                    if args.arches:
                        self.selected_arches = args.arches

        #if no arch or release has been specified on the command line, take it from the configuration
        if self.selected_releases == None:
            self.selected_releases = self.releases
        if self.selected_arches == None:
            self.selected_arches = self.arches

    def _getArches(self, rel):
        """
        Get available arches for current release
        """
        _arches = copy.copy(self.selected_arches)
        if rel == 'el7' and 'i386' in _arches:
            _arches.remove('i386')

        return _arches

    def _checkSources(self):
        """
        Check if sources for current module already exists on disk
        """
        check_sources_cmd = ['spectool', '-l', '-S', self.specfile]
        if args.verbose:
            print(_('Checking if sources exists for module %s') % self.name)
        if args.debug:
            print(_('Command: '), check_sources_cmd)
        process = subprocess.Popen(check_sources_cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        ret = True
        for source in process[0].decode('utf-8').split('\n'):
            if not source == '':
                src = source.split(': ')[1]
                src = src.split('/')[-1]
                if not os.path.exists(src):
                    ret = False
        return ret

    def getSources(self):
        """
        Download modules sources if needeed
        """
        if not(self._checkSources()):
            print(_('Getting sources'))
            sources_cmd = ['spectool', '-g', '-S', self.specfile]
            if args.debug:
                print(sources_cmd)
            cmd_res = subprocess.call(sources_cmd)
            if cmd_res != 0:
                r = input(_('An error occurred getting the sources. Do you want to continue? [Y/n]'))
                if r == 'n':
                    sys.exit(_('Cannot retrieve %s\'s sources.') % self.name)

    def getDeps(self, srpm = None):
        """
        Install build dependencies
    
        In order to use that possibility, you'll have to add (via visudo) the following:
        %mock ALL = NOPASSWD: /usr/bin/yum-builddep *
    
        Or something similar :)
        """
        global args
        #first, let's try to find the src.rpm file (if none has been specified)
        if srpm == None:
            srpm = self._find_srpm()
        deps_cmd = ['sudo', 'yum-builddep', srpm]
        if args.debug:
            print(deps_cmd)
        subprocess.call(deps_cmd)

    def _find_srpm(self):
        found = 0
        srpm = None

        regex = re.compile('.\.src\.rpm')
        for each in os.listdir(self.path):
            if regex.search(each):
                found += 1
                srpm = each
        if found == 0:
            self.rpmbuild('-bs')
            return self._find_srpm()
        elif found > 1:
            srpm = None
            r = input(_('More than one src.rpm has been found... Do you want to remove them all and rebuild srpm? [y/n]'))
            if r == 'y':
                self._clean_srpms()
                self.rpmbuild('-bs')
                return self._find_srpm()
            else:
                sys.exit(_('Multiple SRPMs has been found, I cannot choose the right one, aborting.'))
        else:
            return srpm


    def _clean_srpms(self):
        """
        Clean SRPMs
        """
        regex = re.compile('.\.src\.rpm')
        for each in os.listdir(self.path):
            if regex.search(each):
                print(_('Removing SRPM %s') % each)
                os.remove(os.path.join(self.path, each))

    def _recursive_clean(self, path, rmdir=True, ignore={}):
        """
        Cleans files into specified directory and then delete itself.
    
        With rmdir setted to False, directory itself will not be removed.
        If you specify an ignore-list, all files/directories in that list will not be removed.
        """
        if os.path.exists(path):
            if args.verbose:
                print(_('Cleaning `%s`') % (path))
            for each in os.listdir(path):
                if each in ignore:
                    print(_('%s is in ignore-list, ignored.') % each)
                elif os.path.isdir(os.path.join(path, each)):
                    self._recursive_clean(os.path.join(path, each))
                else:
                    if args.verbose:
                        print(_('Trying to delete `%s`') % (each))
                    os.remove(os.path.join(path, each))
            if rmdir == True:
                os.rmdir(path)

    def rpmbuild(self, action):
        """
        Execute rpmbuild with various arguments
        """
        global args

        #TODO check if sources are present, download them if not
        #How to check if sources are present?? -> spectool -l -S
        self.getSources()
        if args.time:
            self.rpmbuild_cmd.insert(0, 'time')
        local_args = [action]
        if action == '-bc' or action == '-bi' or action == '-bl':
            local_args.append('--short-circuit')
        spec = self.specfile
        if self.path != None:
            spec = os.path.join(self.path, spec)
        local_args.append(spec)
        self.rpmbuild_cmd.extend(local_args)
        if args.debug:
            print(self.rpmbuild_cmd)
        subprocess.call(self.rpmbuild_cmd)

    def mock(self, srpm = None):
        """
        Run mock with adapted configuration
        """
        #first, let's try to find the src.rpm file (if none has been specified)
        if srpm == None:
            if args.keepsrpm == False:
                self._clean_srpms()

            srpm = self._find_srpm()

        mock_cmd = ['mock', '-r']

        if args.time:
            mock_cmd.insert(0, 'time')

        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                if not self.repo.current_arch == 'x86_64' and arch == 'x86_64':
                    print(_('Unable to build %(arch)s packages on a %(current_arch)s machine.') % {'arch': arch, 'current_arch': sef.repo.current_arch})
                else:
                    mock_rel_config = '%s-%s-%s' % (self.mock_config, rel, arch)
                    rel_mock_cmd = []
                    rel_mock_cmd.extend(mock_cmd)
                    rel_mock_cmd.append(mock_rel_config)
                    if self.repo.current_arch != arch:
                        if not rel == 'el5' and arch == 'i386':
                            build_arch = 'i686'
                        else:
                            build_arch = arch
                        rel_mock_cmd.insert(0, 'setarch')
                        rel_mock_cmd.insert(1, build_arch)
                        rel_mock_cmd.extend(['--arch', build_arch])
                    if self.path != None:
                        srpm = os.path.join(self.path, srpm)
                    rel_mock_cmd.append(srpm)
                    if args.debug:
                        print(rel_mock_cmd)
                    subprocess.call(rel_mock_cmd)

    def cleanmock(self):
        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                if os.path.exists(config_opts['resultdir']):
                    if os.path.exists(os.path.join(config_opts['resultdir'], 'build.log')):
                        self._recursive_clean(config_opts['resultdir'], False, {'ignore':'repodata'})

                        print(_('Mock resultdir `%s` cleaned') % config_opts['resultdir'])
                        if config_opts['plugin_conf']['ccache_enable'] == True:
                            self._recursive_clean(config_opts['plugin_conf']['ccache_opts']['dir'], False)
                            print(_('Mock ccache dir `%s` cleaned') % config_opts['plugin_conf']['ccache_opts']['dir'])
                    self.createMockRepo(config_opts['resultdir'], rel)

    def _getMockConfig(self, rel, arch):
        mock_rel_config = '%s-%s-%s' % (self.mock_config, rel, arch)
        config_opts = {}
        config_opts['plugin_conf'] = {}
        config_opts['plugin_conf']['ccache_opts'] = {}
        config_opts['macros'] = {}
        exec(compile(open('/etc/mock/%s.cfg' % mock_rel_config, "r").read(), '/etc/mock/%s.cfg' % mock_rel_config, 'exec'))
        return config_opts
        #%mock users cannot remove the whole directory recursively
        #if os.path.exists(os.path.join(config_opts['basedir'], mock_rel_config)):
        #    _recursive_clean(os.path.join(config_opts['basedir'], mock_rel_config), False)

    def createMockRepo(self, path, rel):
        global args
        createrepo_cmd = ['createrepo', '-q', '-d', '-x', '*.src.rpm', path]
        #EL-5 repositories ave to be created with old checksum default
        if rel.startswith('el') and not self.repo.current_release.startswith('el'):
            createrepo_cmd.extend(['-s', 'sha'])
        if args.verbose:
            print(createrepo_cmd)
        subprocess.call(createrepo_cmd)

    def lint(self):
        global args

        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                if os.path.exists(config_opts['resultdir']):
                    lint_cmd = ['rpmlint']
                    if args.info or args.verbose:
                        lint_cmd.insert(1, '-i')
                    lint_cmd.extend(glob.glob(os.path.join(config_opts['resultdir'], '%s-*.rpm' % self.name)))
                    if args.debug:
                        print(lint_cmd)
                    subprocess.call(lint_cmd)

    def sign(self):
        """
        Sign packages with declared GPG key.
        You have to do this before sending on remote repository.
        """
        global args
        resultdirs = []
        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                resultdirs.append(config_opts['resultdir'])
        packages = []
        for r in resultdirs:
            if args.verbose:
                print(_('Searching for RPMs in %s') % r)
            packages.extend(glob.glob(os.path.join(r, '%s-*.rpm' % self.name)))

        sign_cmd = ['rpmsign', '--addsign']
        sign_cmd.extend(packages)
        if args.debug:
            print(sign_cmd)
        cmd_res = None
        while cmd_res != 0:
            try:
                cmd_res = subprocess.call(sign_cmd)
                if cmd_res != 0:
                    r = input(_('Entered passphrase is incorrect. Do you have another guy? [Y/n]'))
                    if r == 'n':
                        sys.exit(_('Bad GPG passphrase, aborting.'))
            except OSError as e:
                raise OSError(_('Unable to sign package!'))

    def createRepo(self):
        """
        Execute createrepo command on repositories
        We pass '-d' flag so we can further use repoview
        """
        global args
        #FIXME: it works... but it not works. When building a package that need one present in result repository,
        #package is well found (so createrepo was done I guess) ; but throws a "no more mirrors to try" error.
        #Just running createrepo again from same user in relevant directories does the trick :/
        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                if os.path.exists(config_opts['resultdir']):
                    osname = None
                    if rel.startswith('el'):
                        osname = 'el'
                        osrel = rel[2:]
                    else:
                        osname = 'fedora'
                        osrel = rel

                    crepo_cmd = ['createrepo', '-d', '--unique-md-filenames', os.path.join(self.repo.local, osname, osrel, self.repo.dist, arch)]

                    #EL-5 repositories ave to be created with old checksum default
                    if rel.startswith('el') and not self.repo.current_release.startswith('el'):
                        crepo_cmd.extend(['-s', 'sha'])
                        crepo_cmd.extend(['--compress-type', 'bz2'])

                    if args.time:
                        crepo_cmd.insert(0, 'time')

                    if args.debug:
                        print(crepo_cmd)
                    subprocess.call(crepo_cmd)

    def _copyFile(self, src, dest):
        global args
        cp_cmd = ['cp', src, dest]
        if args.time:
            cp_cmd.insert(0, 'time')
        if args.debug:
            print(cp_cmd)
        return subprocess.call(cp_cmd)

    def repoview(self):
        global args

        src = os.path.join(self.repo.repoview_tmpl, 'index_distros.html')
        dest = os.path.join(self.repo.local, 'index.html')
        cp = self._copyFile(src, dest)
        #TODO user should be advised on failure

        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                if os.path.exists(config_opts['resultdir']):
                    if rel.startswith('el'):
                        osname = 'el'
                        osrel = rel[2:]
                    else:
                        osname = 'fedora'
                        osrel = rel

                    src = os.path.join(self.repo.repoview_tmpl, 'index_%s-%s.html' % (osname, osrel))
                    dest = os.path.join(self.repo.local, osname, osrel, 'index.html')
                    cp = self._copyFile(src, dest)


    def copyInRepo(self):
        """
        Copy builded RPMs in the local repository
        """
        global args
        for rel in self.selected_releases:
            for arch in self._getArches(rel):
                config_opts = self._getMockConfig(rel, arch)
                if os.path.exists(config_opts['resultdir']):
                    rpm_sources = glob.glob(os.path.join(config_opts['resultdir'], '%s-*.rpm' % self.name))
                    osname = None
                    if rel.startswith('el'):
                        osname = 'el'
                        osrel = rel[2:]
                    else:
                        osname = 'fedora'
                        osrel = rel
                    mask = re.compile("%s.*(?<!src)\.rpm$" % self.name)
                    rpm_sources = [i for i in os.listdir(config_opts['resultdir']) if mask.match(i)]
                    rpm_target = os.path.join(self.repo.local, osname, osrel, self.repo.dist, arch)
                    if os.path.exists(rpm_target):
                        for rpm in rpm_sources:
                            cp_cmd = ['cp', os.path.join(config_opts['resultdir'], rpm), rpm_target]
                            if args.verbose:
                                cp_cmd.append('-v')
                            if args.debug:
                                print(cp_cmd)
                            subprocess.call(cp_cmd)
                    else:
                        #Should this throw an error?
                        print(_('Local repository path does not exists (%s)') % rpm_target)

                    m = re.search('.*fc(\d(2)).*', str(platform.uname()))
                    dist = m.group(1);

                    if rel == dist and arch == self.repo.current_arch:
                        srpm_sources = glob.glob(os.path.join(config_opts['resultdir'], '%s-*.src.rpm' % self.name))
                        srpm_target = os.path.join(self.repo.local, 'SRPMS')

                        for srpm in srpm_sources:
                            cps_cmd = ['cp', srpm, srpm_target]

                        if args.debug:
                            print(cps_cmd)
                        subprocess.call(cps_cmd)

def _getMainOptions():
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_argument_group('Common options')
    group.add_argument('-t', '--time', dest='time', help='Use time within commands.', action='store_true', default=False)
    group.add_argument('-v', '--verbose', dest='verbose', help='Be a bit more verbose.', action='store_true', default=False)
    group.add_argument('-D', '--debug', dest='debug', help='Activate debug mode (show executes commands, etc.).', action='store_true', default=False)
    group.add_argument('-m', '--modules', dest='modules', help='Module(s) to consider', nargs='+')
    group.add_argument('-k', '--keep-srpm', dest='keepsrpm', help='Keep existing SRPM if any, do no try to rebuild a new one (usefull for big packages)', action='store_true', default=False)
    return parser

def _getMockAndRepoOptions():
    parser = argparse.ArgumentParser(add_help=False)
    group = parser.add_argument_group('Options for mock and repository commands')

    group.add_argument('-l', '--local', dest='local', help='Use "local" release and arch to build. Specific release or arch will override this value.', action='store_true', default=False)
    group.add_argument('-fc', '--release', dest='releases', help='Set release(s)', nargs='+')
    group.add_argument('-a', '--arch', dest='arches', help='Set arch(es)', nargs='+')

    return parser

if '__main__'==__name__:
    parser = argparse.ArgumentParser(description='Fedora Repository Builder')

    #for sub-commands
    subparsers = parser.add_subparsers(dest='action')

    clean_parser = subparsers.add_parser('clean', help='Clean builded RPMs, SRPMs, mock, etc.', parents=[_getMainOptions()])

    #rpmbuild related commands
    rpmbuild_parser = subparsers.add_parser('rpmbuild', help='rpmbuild commands', parents=[_getMainOptions()])
    rpmbuild_group = rpmbuild_parser.add_mutually_exclusive_group()
    rpmbuild_parser.add_argument('-c', '--clean', dest='cleanrpmbuild', help='Clean output from rpmbuild', action='store_true', default=False)
    rpmbuild_group.add_argument('-s', '--sources', help='Download package sources', action='store_true', default=False)
    rpmbuild_group.add_argument('-d', '--deps', help='Install required dependencies', action='store_true', default=False)
    rpmbuild_group.add_argument('-bp', '--prepare', help='Exectute rpmbuild -bp', action='store_const', dest='rpmbuildaction', const='-bp')
    rpmbuild_group.add_argument('-bc', '--compile', help='Execute rpmbuild -bc -short-circuit', action='store_const', dest='rpmbuildaction', const='-bc')
    rpmbuild_group.add_argument('-bi', '--install', help='Execute rpmbuild -bi --short-circtui', action='store_const', dest='rpmbuildaction', const='-bi')
    rpmbuild_group.add_argument('-bl', '--listcheck', help='Execute rpmbuild -bl --short-circuit', action='store_const', dest='rpmbuildaction', const='-bl')
    rpmbuild_group.add_argument('-bb', '--build', help='Do build (with rpmbuild)', action='store_const', dest='rpmbuildaction', const='-bb')
    rpmbuild_group.add_argument('-bs',  '--build-srpm', help='Build SRPM', action='store_const', dest='rpmbuildaction', const='-bs')

    #mock related commands
    mock_parser = subparsers.add_parser('mock', help='Execute mock', parents=[_getMainOptions(), _getMockAndRepoOptions()])
    mock_parser.add_argument('-c', '--clean', dest='cleanmock', help='Clean mock resultdir and execute the first createrepo', action='store_true', default=False)
    mock_parser.add_argument('-s', '--srcrpm', help='SRPM file to use')

    #rpmlint related commands
    lint_parser = subparsers.add_parser('lint', help='Run rpmlint on mock generated RPMs', parents=[_getMainOptions(), _getMockAndRepoOptions()])
    lint_parser.add_argument('-i', '--info', help='Add -i argument to rpmlint call', action='store_true', default=False)

    #repository related commands
    repo_parser = subparsers.add_parser('repo', help='Execute all repository related operations (packages sign, copy to local repo, createrepo, repoview). This command does not include repository synchronization.', parents=[_getMainOptions(), _getMockAndRepoOptions()])
    repo_group = repo_parser.add_mutually_exclusive_group()
    repo_group.add_argument('-cr', '--createrepo', help='Execute createrepo on each repository', action='store_true', default=False)
    repo_group.add_argument('-r', '--repoview', help='Execute repoview on each repository', action='store_true', default=False)
    repo_group.add_argument('-c', '--copy', dest='copy', help='Copy builded RPMs to local repository', action='store_true', default=False)
    repo_group.add_argument('-s', '--sign', help='Sign packages with your GPG key', action='store_true', default=False)
    repo_group.add_argument('-sy', '--sync', dest='sync', help='Synchronize remote repository with local. This will synchronize the whole repository, regardless of distors/archs configured for a specific package.', action='store_true', default=False)
    repo_group.add_argument('-co', '--completerepo', help='Execute all repository actions (excepted sync)', action='store_true', default=False)

    #repository synchronization
    sync_parser = subparsers.add_parser('sync', help='repository synchronisation', parents=[_getMainOptions()])
    sync_parser.add_argument('-bw', '--bwlimit', help='Limit bandwith on sync action (ie. 200k)')

    complete_parser = subparsers.add_parser('complete', help='Complete build (download sources, make SRPM, run mock, create the whole repository). This command does not include repository synchronization.', parents=[_getMainOptions(), _getMockAndRepoOptions()])

    subparsers.add_parser('credits', help='Show credits')

    args = parser.parse_args()

    if args.action == 'credits':
        print(credits)
        sys.exit(0)

    #if modules has been specified on the command line, we'll work with these ones
    if args.modules:
        repo = Repository(args.modules, True)
    else:
        repo = Repository(None, True)

    #cleanall should be the first operation
    if args.action == 'clean':
        repo._doCleanAllAction()
    else:
        #requested action (required)
        if args.action == 'complete':
            repo._doCompleteAction()
        elif args.action == 'repo':
            repo._doRepoAction()
        elif args.action == 'sync':
            repo._doSyncAction()
        elif args.action == 'rpmbuild':
            repo._doRpmbuildAction()
        elif args.action == 'mock':
            repo._doMockAction()
        elif args.action == 'lint':
            repo._doLintAction()
