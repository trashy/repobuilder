#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 Johan Cwiklinski <johan AT x-tnd DOT be>
#
# This file is part of repobuilder.
#
# repobuilder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# repobuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with repobuilder.  If not, see <http://www.gnu.org/licenses/>.

"""
Fedora Repository Builder - ncurses application
"""

import os, argparse, urwid

class NRepoBuilder:

    def _modulesList(self):
        """
        List modules contained in the repo
        """
        ignore_list = ['.hg', 'common', '.settings', '.pydevproject', 'mock_config', 'repoview_ulysses_theme', '.project', '.hgignore', '.hgtags']
        modules = []
        dirs = os.listdir(os.path.dirname(self.directory))
        dirs.sort()
        for each in dirs:
            if not each in ignore_list:
                #modules.append(urwid.Text(each))
                #modules.append(each)
                modules.append(urwid.AttrWrap(urwid.CheckBox(each),'buttn','buttnf'))
        return modules

    def __init__(self, modules):
        self.modules = modules
        #store current directory 
        self.directory = os.getcwd()
        self.blank = urwid.Divider()
        self.text_header = 'Fedora Repository Builder'
        self.header = urwid.AttrWrap(urwid.Text(self.text_header), 'header')
        self.palette = [
            ('streak', 'black', 'light gray', 'standout,underline'),
            ('header', 'light gray', 'dark blue', 'bold'),
            ('bg', 'black', 'default'),
            ('reveal focus', 'black', 'dark cyan', 'standout')
        ]

        #listbox_content = [
        #                   self.blank,
        #                   urwid.Text('Present modules:'),
        #                   self.blank
        #                   ]
        #listbox_content.extend(self._modulesList())
        #listbox_content = [urwid.AttrWrap(urwid.CheckBox(mod),'buttn','buttnf') for mod in self._modulesList()]

        #mods = self._modulesList()
        ##urwid.AttrWrap(urwid.CheckBox(each),'buttn','buttnf')
        #listbox_content = urwid.Padding(urwid.GridFlow(
        #    [urwid.AttrWrap(urwid.RadioButton([],
        #        txt), 'buttn','buttnf') 
        #        for txt in mods],
        #    50, 3, 1, 'left') ,
        #    ('fixed left',4), ('fixed right',3)),

        modules = urwid.Pile([urwid.Text('Present modules:')] + self._modulesList(), focus_item=1)
        col = urwid.Columns([('fixed',16,None), modules], 3, focus_column=1)
        l = [None, urwid.Divider(), col]

        self.listbox = urwid.ListBox(urwid.SimpleListWalker(l))
        self.frame = urwid.Frame(urwid.AttrWrap(self.listbox, 'body'), header=self.header)

    def main(self):
        """Run the program."""
        screen = urwid.raw_display.Screen()
        urwid.MainLoop(self.frame, self.palette, screen, unhandled_input=self.unhandled_input).run()

    def unhandled_input(self, k):
        # update display of focus directory
        print k
        if k in ('q','Q'):
            raise urwid.ExitMainLoop()
        #elif k == 'left':
        #    self.move_focus_to_parent()
        #elif k == '-':
        #    self.collapse_focus_parent()
        #elif k == 'home':
        #    self.focus_home()
        #elif k == 'end':
        #    self.focus_end()

def main(modules):
    NRepoBuilder(modules).main()

if '__main__'==__name__:
    parser = argparse.ArgumentParser()
    modules = None

    parser.add_argument('-m', '--modules', nargs='*', help='Module(s) to work with. You can specify multiple module separating them by a space.')
    args = parser.parse_args()

    if args.modules:
        modules = args.modules

    main(modules)